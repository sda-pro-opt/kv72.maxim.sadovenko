Після запуску програми, буде запрошено написати назву файла(БЕЗ РОЗШИРЕННЯ), далі виконається лексичний аналіз, результат якого можна буде
подивитись в новому файлі filename_generated.txt.

Варіант - 11.

Граматика:

```
1. <signal-program> --> <program>
2. <program> --> PROGRAM <procedure-identifier> ;
		<block>.
3.	<block> --> <declarations> BEGIN <statements-
list> END
4. <declarations> --> <label-declarations>
5. <label-declarations> --> LABEL <unsigned-
integer> <labels-list>; |
<empty>
7. <labels-list> --> , <unsigned-integer>
<labels-list> |
<empty>
8. <statements-list> --> <statement> <statements-
list> |
<empty>
8. <statement> --> <unsigned-integer> :
<statement> |
	GOTO <unsigned-integer> ; |
	LINK <variable-identifier> , <unsigned-
	integer> ; |
	IN <unsigned-integer>; |
	OUT <unsigned-integer>;
9. <variable-identifier> --> <identifier>
10. <procedure-identifier> --> <identifier>
11. <identifier> --> <letter><string>
12. <string> --> <letter><string> |
	<digit><string> |
	<empty>
13.<unsigned-integer> --> <digit><digits-string>
14. <digits-string> --> <digit><digits-string> |
15. <empty><digit> --> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
16. <letter> --> A | B | C | D | ... | Z
```